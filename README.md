# Dlg Engineer Exercise

## About
This is an implementation of the Dlg Engineer Exercise implemented by [Sul Aga](http://www.sulhome.com)

## Architecture

The app was created using Angular CLI. The following components/services were created to fulfil the task

* `PanelComponent`: This component will host the FAQ question. It also inlude the logic to expand and collapse its body

* `AccordionComponent`: This component will host a collection of panels and it will orchestrate which one is opened and which is closed

* `HomeComponent`: This component act as a container component for Accordion and Panel

 * `DataService`: A service to fetch the FAQs from supplied file
 
## Testing
Unit testing was used for all included components to test their logic.

e2e tests was also used to test the full scenario

## Responsive design
Bootstrap framework was used as a base for the css used in the application.
The application was tested in desktop and mobile view in Chrome and it is working as expected.

## Improvements

The provided implementation is a basic implementation to fulfil the task but there is room for more improvements:
* Accordion component could receive a configuration object to customise the behaviour. For example allow more than one panel to be opened at the same time
* The PanelComponent could be further divided into sub components. For example we can have icon button component
