import { AppPage } from './app.po';
import {browser, by, element, logging} from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display faqs', () => {
    page.navigateTo();
    const faqs = element.all(by.className('card'));
    const firstQuestion = element.all(by.className('card-header')).first();
    element.all(by.className('icon-btn')).first().click();
    expect(firstQuestion.getText()).toContain('What vehicles are covered?');
    expect(faqs.count()).toBe(5);
  });

  it('should expand faq when icon clicked', () => {
    page.navigateTo();
    expect(element(by.className('card-body')).isPresent()).toBe(false);
    element.all(by.className('card')).first().all(by.className('icon-btn')).first().click();
    expect(element(by.className('card-body')).isPresent()).toBe(true);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
